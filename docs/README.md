# Welkom bij het Dustpuppy project :smile:

Bij de Nederlanse Linux Gebruikers Groep ([NLLGG](https://nllgg.nl)) zijn we een soort "crowdthinking" project om te bepalen hoe de ideale server voor thuis er uit zou moeten zien. Zowel van binnen als van buiten. Hier kun je lezen over de status en meedenken. Het project komt voort uit te ervaring van @aswen dat hij vaak hoorde over wat collega's en vrienden thuis voor mooie server hadden gebouwd. Hij wil proberen al die ervaring te combineren en zo de ultieme homeserver te ontwerpen.

## Doel
Het doel is om hier te documenteren hoe de ideale server voor thuis er uit moet zien. Maar ook is het belangrijk alle overwegen die aan keuzes ten grondslag liggen vast te leggen. De documentatie hier zal, na eerste oplevering, geen statisch document zijn. Veranderende prijzen kunnen van invloed zijn op keuzes voor componenten en ervaring met bepaalde onderdelen natuurlijk ook.

## Communicatie
Voor dit project is een [mailinglist](https://lists.nllgg.nl/listinfo/homeserverproject) in het leven geroepen. Daar vinden aankondigen op plaats met betrekking to bijeenkomsten en ander nieuws. De mailinglist is *niet* bedoeld om inhoudelijke discussie op te voeren. Aanmelden kan door te mailen naar [homeserverproject-join@lists.nllgg.nl](mailto:homeserverproject-join@lists.nllgg.nl).
Er is een [archief](https://lists.nllgg.nl/pipermail/homeserverproject) van deze mailinglist. Dit archief is te lezen door iedereen op internet. Wees dus terughoudend met het posten van privé informatie. Iets van internet af krijgen is een stuk moeilijker dan er op.
Inhoudelijke discussie voeren we in de [issues](https://gitlab.com/nllgg/dustpuppy/issues). Uiteindelijk moeten de issues dan een document opleveren dat wordt opgenomen in deze repository. Het is belangrijk dat discussies over één onderwerp gaan en leesbaar blijven. Voorkom al te veel quoten en probeer duidelijk maar bondig te formuleren wat je bedoelt.

## Eisen
We hebben een brainstorm gehouden over wat we eigenlijk verwachten van een server voor thuis. [Hier](eisen.md) kun je lezen wat we willen dat de homeserver gaat kunnen.

## Proces
Tijdens de tweede bijeenkomst bespraken we het proces van dit project. We hebben keuzes gemaakt over hoe we gaan samenwerken. In het [procesdocument](proces.md) kun je lezen wat we hebben besloten.

## Hardware
We gaan om te beginnen werken aan twee ontwerpen: een zware server voor het echte werk en een ultiem licht ding dat écht weinig stroom verbruikt. Natuurlijk moet het zware model ook zuinig zijn trouwens. We documenteren beide modellen in [de hardware directory](hardware/README.md).

## Distributie
De keuze voor een [ditributie](distributie.md) werken we ook uit. Het moet gedocumenteerd zijn waarom we voor een bepaalde distro kiezen. Over een tijd kan er van alles anders zijn en dan moeten we niet ons af vragen waarom we ook weer kozen voor degene die het gaat worden.

## Installatiemethode
We willen iets dat, na montage, met minimale middelen te installeren is. We zullen daarom een [methode](installatie.md) uitdenken en daarna bouwen die dat mogelijk maakt.

## Configuratiemanagement
Samen bouwen we een [configuratie management systeem](configuratie_management.md) dat modulair opgebouwd zal worden. We moeten zorgen voor een makkelijk te beheren privedeel waar mensen een configbestand kunnen maken dat vervolgens door de modules van het configuratie management systeem zal worden gebruikt om verder alles te configureren.

## Inrichting
De teams die werken aan distributie, installatie en configuratie maken keuzes over hoe het systeem [in te richten](inrichting.md). (disk indeling enzovoort)

## Dustpuppy?
Dustpuppy is een [personage](http://www.userfriendly.org/cartoons/dustpuppy/) uit de strips van [Userfriendly](http://userfriendly.org). Het werd voor het eerst [geïntroduceerd](http://ars.userfriendly.org/cartoons/?id=19971203) in een strip waarbij een (oude) computer geopend werd waaruit dit figuur gekropen kwam.
